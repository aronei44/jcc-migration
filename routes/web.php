<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');
Route::get('/welcome', 'AuthController@welcome');

Route::get('/', function () {
	return view('layout.index');
});
Route::get('/table', function () {
	return view('layout.table');
});
Route::get('/data-table', function () {
	return view('layout.data-table');
});
